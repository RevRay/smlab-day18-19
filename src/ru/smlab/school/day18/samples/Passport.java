package ru.smlab.school.day18.samples;

import java.util.Comparator;

public class Passport {
    String name;
    String id;

    public Passport(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public int hashCode(){
        return name.length() + id.length();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj.getClass() == this.getClass())) {
            return false;
        }
        Passport another = (Passport) obj;
        if (this.hashCode() != another.hashCode()) {
            return false;
        }
        boolean objectsEqual = this.name.equals(another.name) && this.id.equals(another.id);
        return objectsEqual;
    }

    public void updateId(String newId) {
        this.id = newId;
    }

    @Override
    public String toString() {
        return "Паспорт " +
                "на имя '" + name + '\'' +
                ", номер '" + id + '\'' +
                '}';
    }

}
