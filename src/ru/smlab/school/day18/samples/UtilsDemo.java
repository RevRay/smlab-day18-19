package ru.smlab.school.day18.samples;

import java.util.*;

public class UtilsDemo {
    public static void main(String[] args) {
        List<Passport> passports = new LinkedList<>();
        passports.add(new Passport("Плюшкин Иван", "4512 903331"));
        passports.add(new Passport("Батонов Федор", "4511 773071"));
        passports.add(new Passport("Кулебякин Игорь", "4512 264995"));

//        System.out.println(passports);

        Iterator<Passport> iter = passports.iterator();

        if (iter.hasNext()) {
            System.out.println(iter.next());
        }

        System.out.println(passports);
    }
}
