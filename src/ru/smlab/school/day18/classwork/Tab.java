package ru.smlab.school.day18.classwork;

public class Tab {
    String tabName;
    boolean playsSound;

    public Tab(String tabName, boolean playsSound) {
        this.tabName = tabName;
        this.playsSound = playsSound;
    }

    @Override
    public String toString() {
        return "{" + tabName + '}';
    }
}
