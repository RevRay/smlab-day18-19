package ru.smlab.school.day19.equals;

import ru.smlab.school.day18.samples.Passport;

import java.util.HashSet;
import java.util.Set;

public class EqualsDemo {
    public static void main(String[] args) {
        Passport p1 = new Passport("Федот", "34r234");
        Passport p2 = new Passport("Федот", "34r234");
        Passport p3 = new Passport("Игорь", "34выаваы");
        Passport p5 = null;
        Passport p4 = new Passport("Федот", "9999000");

//        System.out.println(p1 == p2);
        System.out.println(p1.equals(p1));
        System.out.println(p1.equals(p2));
        System.out.println(p1.equals(p3));
        System.out.println(p1.equals(p4));
        System.out.println(p1.equals(p5));


        Set<String> passportSet = new HashSet<>();
        passportSet.add("Федот");
        passportSet.add("Федот");
        passportSet.add("Федот231");
        passportSet.add("Федот");
        passportSet.add("Кристина");
        passportSet.add("Кристина3423");
        passportSet.add("Кристина4234");
        passportSet.add("Игорь");
        passportSet.add("Игорь");
        System.out.println("my set:" + passportSet);

        System.out.println(passportSet.contains("Игорь1"));
    }
}
