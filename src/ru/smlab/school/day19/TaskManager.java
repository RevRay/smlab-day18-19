package ru.smlab.school.day19;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
Разработать следующий функционал:
1. добавление новых задач
2. удалить задачу
3. вывести на экран все задачи
4. посчитать количество сделанных задач
5. посчитать количество несделанных задач
6. удалить из списка дубликаты задач
7. поиск в списке задач по слову
 */
public class TaskManager {
    static List<Task> tasks = new ArrayList<>();

    public static void main(String[] args) {
        String input = "Покормить кота";
        addTask(input);
        addTask("оплатить квитанции");
        addTask(input);
        addTask("купить обезболивающее");
        addTask("купить обезболивающее");
        System.out.println(tasks);
        removeTask(1);
        printAllTasks();

        System.out.println("Завершенные задачи: " + countCompletedTasks());
        System.out.println("Незавершенные задачи: " + countPendingTasks());
        clearDuplicates();
        printAllTasks();
    }

    public static void addTask(String taskDescription) {
        Task newTask = new Task(taskDescription);
        tasks.add(newTask);
    }

    public static void removeTask(int i) {
        tasks.remove(i);
    }

    public static void printAllTasks() {
        System.out.println("=== ALL TASKS ===");
//        Iterator<Task> iter = tasks.iterator();
//        while (iter.hasNext()) {
//            System.out.println(iter.next());
//        }
        for (Task t : tasks) {
            System.out.println(t);
        }
    }

    public static int countCompletedTasks() {
        int completedCounter = 0;
        for (Task t : tasks) {
            if (t.isCompleted) {
                completedCounter++;
            }
        }
        return completedCounter;
    }

    public static int countPendingTasks() {
        return tasks.size() - countCompletedTasks();
    }

    public static void clearDuplicates() {
        for(int i = 0; i < tasks.size(); i++) {
            String s = tasks.get(i).taskName;
            for(int j = i + 1; j < tasks.size(); j++) {
                if (tasks.get(j).taskName.equals(s)) {
                    tasks.remove(j);
                }
            }
        }
        //        Iterator iter
//        while () {
//            int currentIndex = 0;
//            String description = task.taskName;
//            for (int i = currentIndex; i < tasks.size(); i++) {
//                if (tasks.get(i).taskName.equals(description)) {
//                    tasks.remove(i);
//                }
//            }
//            currentIndex++;
//        }
    }
}
