package ru.smlab.school.day19;

import ru.smlab.school.day18.samples.Passport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> l1 = List.of(1);
        System.out.println(l1.getClass().getName());
        List<Integer> listA = new ArrayList<>();
        listA.add(1);
        listA.add(100);
        listA.add(105);
        listA.add(555);
        listA.set(1, 99);
        System.out.println(listA);

        List<Integer> listA2 = Arrays.asList(1, 100, 105);
        listA2.set(1, 99);
//        listA2.add(555);

        List<Integer> listA3 = List.of(1, 100, 105);
//        listA3.set(1, 99);
        listA3.add(555);
    }

//    public static void main(String[] args) {
//        List<Passport> passports = new ArrayList<>();
//        passports.add(new Passport("Иванов", "100"));
//        passports.add(new Passport("Иванов11", "1656500"));
//        passports.add(new Passport("Иванов22", "10454450"));
//
//
//    }
}
