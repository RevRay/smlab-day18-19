package ru.smlab.school.day18.classwork;

import java.util.*;

/**
 * ### Задание в классе 18.0.2
 * Создать массив цветов (минимум 7 позиций). ""
 * Сконвертировать массив в список (ArrayList).
 * Вывести на экран весь список с помощью итератора.
 * Отсортировать список по алфавиту.
 * Заменить элементы списка на позициях 1, 3, 5 на аналогичные, но с приставкой + "Dark ".
 * Вывести на экран весь список с помощью for-each цикла.
 * Получить под-список с 1 по 5 элементы включительно.
 * Написать метод, который поменяет местами 1 и 4 элементы межды собой.
 * Получить элемент с индексом 3, сохранить в переменную а1.
 * Проверить, содержится ли объект из переменной а1 в коллекции.
 * Удалить все элементы, содержащие букву 'o'
 * Превратить список в массив.
 */
public class ClassTask {
    public static void main(String[] args) {
        String[] colors = {"красный", "оранжевый", "желтый", "зеленый", "голубой", "синий", "фиолетовый"};
        List<String> list = new ArrayList<>(Arrays.asList(colors));

        Iterator<String> iter = list.iterator();
        //next() hasNext()
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        Collections.sort(list); //а-я 1-9
        System.out.println(list);
        System.out.println(list);

        //Заменить элементы списка на позициях 1, 3, 5 на аналогичные, но с приставкой + "Dark ".
        String newElement = "Dark " + list.get(1);
        list.set(1, newElement);

        list.set(3, "Dark " + list.get(3));
        list.set(5, "Dark " + list.get(5));
        System.out.println(list);

// * Вывести на экран весь список с помощью for-each цикла.
        for (String color : list) {
            System.out.println(color);
        }

        // * Получить под-список с 1 по 5 элементы включительно.
        List<String> subList = list.subList(1, 6);
     //* Написать метод, который поменяет местами 1 и 4 элементы межды собой.
        System.out.println(subList);
//        swapElements(subList, 1, 4);
        Collections.swap(subList, 1, 4);
        System.out.println(subList);

        //Получить элемент с индексом 3, сохранить в переменную а1.
        String a1 = subList.get(3);
                //* Проверить, содержится ли объект из переменной а1 в коллекции.

        System.out.println(subList.contains(a1));
        System.out.println(subList.contains("фуксия"));

        // * Удалить все элементы, содержащие букву 'o'
        for (String s : subList) {
            if (s.contains("о")) {
                subList.remove(s);
            }
        }
        int i =0;
        for(/*перем счет*/ ; /*условие продолжения цикла*/ ; i++){
            System.out.println("fdsf");
            break;
        }
//        System.out.println(subList);
//        // * Превратить список в массив.
//
//        Object[] resultArray = subList.toArray();
//        System.out.println(resultArray);
    }

    private static void swapElements(List<String> subList, int i, int j) {
        String temp = subList.get(i);
        subList.set(i, subList.get(j));
        subList.set(j, temp);
    }
}
