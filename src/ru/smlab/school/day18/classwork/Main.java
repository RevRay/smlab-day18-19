package ru.smlab.school.day18.classwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String[] array = {"Bob", "Dylan", ""};

        ArrayList<String> list = new ArrayList<String>();
        list.add("Bob"); //0
        list.add("Dylan"); //1
        list.add("Margareth"); //2

        System.out.println(list);

        list.add(2, "Judy");
        System.out.println(list);

        System.out.println(list.get(3));

        list.set(2, "Mark");
        System.out.println(list);

//        list.remove(1);
        list.remove("Dylan");
        System.out.println(list);
    }
}
