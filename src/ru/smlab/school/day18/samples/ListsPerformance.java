package ru.smlab.school.day18.samples;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListsPerformance {

    public static void main(String[] args) {
        List<String> testList = new ArrayList<>();
        testList.add("Java");
        testList.add("Kotlin");
        testList.add("Groovy");
        testList.add("Clojure");
        testList.add("Scala");

        List<String> testList2 = new LinkedList<>();
        testList2.add("Java");
        testList2.add("Kotlin");
        testList2.add("Groovy");
        testList2.add("Clojure");
        testList2.add("Scala");

        System.out.println(testList2);

        measureInsertTime(testList, 100000);
        measureInsertTime(testList2, 100000);

        measureGetTime(testList, 1000000);
        measureGetTime(testList2, 1000000);




    }

    public static void measureInsertTime(List<String> list, int count) {

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            list.add(3, "simple_string_" + i);
        }
        long endTime = System.currentTimeMillis();

        System.out.println(
                String.format(
                        "Время работы для структуры '%s' составило %d мс.",
                        list.getClass().getSimpleName(),
                        endTime - startTime
                )
        );

    }

    public static void measureGetTime(List<String> list, int count) {

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            list.get(list.size() - 1);
        }
        long endTime = System.currentTimeMillis();

        System.out.println(
                String.format(
                        "Время работы для структуры '%s' составило %d мс.",
                        list.getClass().getSimpleName(),
                        endTime - startTime
                )
        );

    }
}
