package ru.smlab.school.day18.samples;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionsComparison {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("Небольшой");
        arrayList.add("но");
        arrayList.add("счастливый");
        arrayList.add("набор");
        arrayList.add("данных");

        List<String> linkedList = new LinkedList<>();
        linkedList.add("Небольшой");
        linkedList.add("но");
        linkedList.add("счастливый");
        linkedList.add("набор");
        linkedList.add("данных");

        workWithList(arrayList);
        workWithList(linkedList);
    }

    public static void workWithList(List<String> list) {
        System.out.println(String.format("\nвывод списка '%s' через foreach: ", list.getClass().getSimpleName()));
        for (String s :
                list) {
            System.out.print(s + " ");
        }
    }
}
