package ru.smlab.school.day19.universityutil;

import javax.naming.CompositeName;
import java.util.List;

public class Student {
    String fio;
    List<Integer> scores; //4,5,5
    int absenceCount;
    double avgScore;

    public double recalculateAvgScore(){
        double scoreSum = 0.0;
        for (Integer score : scores){
            scoreSum += score;
        }
        double avgScore = scoreSum / scores.size();
        this.avgScore = avgScore;
        return avgScore;
    }

}
