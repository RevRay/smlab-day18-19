package ru.smlab.school.day18.classwork;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CopyDemo {
    public static void main(String[] args) {
        List<String> oldList = new ArrayList<>();
        oldList.add("Java");
        oldList.add("Kotlin");
        oldList.add("Groovy");
        oldList.add("Clojure");
        oldList.add("Scala");

        List<String> newList = new ArrayList<>(oldList);

        Iterator<String> iter = oldList.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.hasNext());
        }
    }
}
